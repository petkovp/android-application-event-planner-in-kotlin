package com.androidteam.eventplanner

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.util.Log

/**
 * The databasehelper that implements the SQLiteOpenHelper that creates the tables
 */
class DatabaseHelper(var context: Context) : SQLiteOpenHelper(context, dbName, null, 1) {

    companion object {
        const val dbName = "MY DATABASE"
        const val tableName = "EventTable"
        const val eventIDCOL = "eventID"
        const val eventName = "eventName"
        const val eventDesc = "eventDesc"
        const val eventImportance = "eventImportance"
        const val eventDate = "eventDate"
        const val eventReminder = "eventReminder"
    }

    /**
     * creates the table for the events
     */
    override fun onCreate(db: SQLiteDatabase?) {
        Log.i("TABLE CREATE", "OnCreate")

        val query = "CREATE TABLE " + tableName +
                " ( " + eventIDCOL + " INTEGER PRIMARY KEY AUTOINCREMENT, " + eventName + " VARCHAR(256), " +
                "" + eventDesc + " VARCHAR(256), " + eventImportance + " VARCHAR(256)," +
                " " + eventDate + " VARCHAR(256), " + eventReminder + " VARCHAR(256) )"
        db?.execSQL(query)
    }

    /**
     * recreates table
     */
    override fun onUpgrade(db: SQLiteDatabase?, p1: Int, p2: Int) {
        Log.i("TABLE UPGRADE", "OnUpgrade")

        db?.execSQL("DROP TABLE $tableName")
        onCreate(db)
    }
}