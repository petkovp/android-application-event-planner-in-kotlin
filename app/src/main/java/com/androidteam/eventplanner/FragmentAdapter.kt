package com.androidteam.eventplanner

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter

/**
 * Fragment adapter implementing FragmentPagerAdapter
 */
class FragmentAdapter(val context: Context, val fm: FragmentManager) :
    FragmentPagerAdapter(fm, BEHAVIOR_RESUME_ONLY_CURRENT_FRAGMENT) {

    private val tabTitles = arrayOf(context.getString(R.string.list_event),context.getString(R.string.add_event))

    /**
     * return the amount of tabs
     */
    override fun getCount(): Int {
        return tabTitles.size
    }

    /**
     * gets the listevents fragment and createvent fragment
     */
    override fun getItem(pos: Int): Fragment {
        var fragment: Fragment? = null
        when (pos){
            0 -> return ListEventsFragment()
            1 -> return CreateEventFragment()
        }
        kotlin.requireNotNull(fragment) {"Fragment was null"}
        return fragment
    }

    /**
     * returns the title of the tab
     */
    override fun getPageTitle(position: Int): CharSequence? {
        return this.tabTitles[position]
    }
}