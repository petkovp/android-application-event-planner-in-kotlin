package com.androidteam.eventplanner;
import java.util.Date;

/**
 * Event Object class
 */
public class EventObject {

    private String eventName;
    private String eventDesc;
    private String eventImp;
    private Date eventDate;
    private Date reminderDate;
    private Integer eventID;

    /**
     * Custom constructor
     *
     * @param eventName
     * @param eventDesc
     * @param eventImp
     * @param eventDate
     * @param reminderDate
     */
    public EventObject(String eventName, String eventDesc, String eventImp, Date eventDate, Date reminderDate) {
        this.eventName = eventName;
        this.eventDesc = eventDesc;
        this.eventImp = eventImp;
        this.eventDate = eventDate;
        this.reminderDate = reminderDate;
        this.eventID = eventID;
    }

    public String getEventName() {
        return this.eventName;
    }

    public String getEventDesc() {
        return eventDesc;
    }

    public String getEventImp() {
        return eventImp;
    }

    public Date getEventDate() {
        return eventDate;
    }

    public Date getReminderDate() {
        return reminderDate;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public void setEventDesc(String eventDesc) {
        this.eventDesc = eventDesc;
    }

    public void setEventImp(String eventImp) {
        this.eventImp = eventImp;
    }

    public void setEventDate(Date eventDate) {
        this.eventDate = eventDate;
    }

    public void setReminderDate(Date reminderDate) {
        this.reminderDate = reminderDate;
    }

    public Integer getEventID() {
        return eventID;
    }

    public void setEventID(Integer eventID) {
        this.eventID = eventID;
    }
}