package com.androidteam.eventplanner

import android.app.Activity
import android.content.ContentValues
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import java.text.SimpleDateFormat
import java.util.*

/**
 * The secondary activity from intent that is used to edit an event or delete it
 */
class EditEventActivity : AppCompatActivity() {
    private lateinit var nameText: EditText
    private lateinit var descText: EditText
    private lateinit var impSpinner : Spinner
    private lateinit var datePicker: DatePicker
    private lateinit var timePicker: TimePicker
    private lateinit var checkBoxReminder: CheckBox
    private lateinit var reminderDatePicker: DatePicker
    private lateinit var reminderTimePicker: TimePicker
    private lateinit var id : String
    private lateinit var name : String
    private lateinit var desc : String
    private lateinit var imp : String
    private lateinit var date : Date
    private lateinit var reminder : Date

    /**
     * Set up the activity
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.edit_event_layout)
    }

    /**
     * Sets the event view with the event data
     * REQUIRES: API 23+
     */
    @RequiresApi(Build.VERSION_CODES.M)
    public override fun onStart() {
        super.onStart()
        nameText = findViewById(R.id.eventNameIdEdit)
        descText = findViewById(R.id.eventDescIdEdit)
        impSpinner = findViewById(R.id.importance_spinnerEdit)
        this.let {
            ArrayAdapter.createFromResource(it, R.array.importance_array, android.R.layout.simple_spinner_item).also { adapter ->
                // Specify the layout to use when the list of choices appears
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                // Apply the adapter to the spinner
                impSpinner.adapter = adapter
            }
        }
        datePicker = findViewById(R.id.datePickerIdEdit)
        timePicker = findViewById(R.id.timePickerIdEdit)
        timePicker.setIs24HourView(true)
        checkBoxReminder = findViewById(R.id.checkboxReminderIdEdit)
        reminderDatePicker = findViewById(R.id.reminderDatePickerIdEdit)
        reminderTimePicker = findViewById(R.id.reminderTimePickerIdEdit)
        reminderTimePicker.setIs24HourView(true)

        val data = intent.extras
        id = data!!.getString("id").toString()
        name = data!!.getString("name").toString()
        desc = data!!.getString("desc").toString()
        imp = data!!.getString("imp").toString()
        date = SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(data!!.getString("date").toString())
        date.year = date.year + 1900
        displayUI()

        if (data.containsKey("reminder")) {
            reminder = SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(data!!.getString("reminder").toString())
            reminder.year = reminder.year + 1900
            checkBoxReminder.isChecked = true;

            reminderDatePicker.updateDate(reminder.year, reminder.month, reminder.date)
            reminderTimePicker.hour = reminder.hours
            reminderTimePicker.minute = reminder.minutes
        }
        else {
            // Since there are no reminders for this event,
            // you set the reminder DatePicker and TimePicker to the event date's date and time
            reminderDatePicker.updateDate(date.year, date.month, date.date)
            reminderTimePicker.hour = date.hours
            reminderTimePicker.minute = date.minutes
        }

    }

    /**
     * Updates the UI with the event data that is being edited
     * REQUIRES: API 23+
     */
    @RequiresApi(Build.VERSION_CODES.M)
    fun displayUI(){
        nameText.setText(name)
        descText.setText(desc)
        impSpinner.setSelection(findSpinnerValue())

        datePicker.updateDate(date.year, date.month, date.date)
        timePicker.hour = date.hours
        timePicker.minute = date.minutes
    }

    /**
     * Gets the selected importance by the user
     */
    fun findSpinnerValue() : Int {
        val arr = resources.getStringArray(R.array.importance_array)
        for (i in arr.indices) {
            Log.i("POSITION", "" + i)
            if (arr[i].equals(imp)) {
                return i
            }
        }
        return -1
    }

    /**
     * Event listener when click on button updates the event
     * REQUIRES: API 23+
     */
    @RequiresApi(Build.VERSION_CODES.M)
    fun updateEvent(view: View) {
        if (nameText.text.toString().isEmpty()) {
            Toast.makeText(this, R.string.required_name, Toast.LENGTH_SHORT).show()
        }
        else {
            var cv = ContentValues()
            cv.put(DatabaseHelper.eventName, nameText.text.toString())
            cv.put(DatabaseHelper.eventDesc, descText.text.toString())
            cv.put(DatabaseHelper.eventImportance, impSpinner.selectedItem.toString())

            date = Date(datePicker.year - 1900, datePicker.month, datePicker.dayOfMonth, timePicker.hour, timePicker.minute)
            cv.put(DatabaseHelper.eventDate, SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(date))

            // If true, then include reminder
            if (checkBoxReminder.isChecked) {
                reminder = Date(reminderDatePicker.year - 1900, reminderDatePicker.month, reminderDatePicker.dayOfMonth,
                    reminderTimePicker.hour, reminderTimePicker.minute)
                cv.put(DatabaseHelper.eventReminder, SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(reminder))

                if (reminder.after(date)) {
                    Toast.makeText(this, R.string.date_warning, Toast.LENGTH_SHORT).show()
                    return
                }
            } else {
                cv.put(DatabaseHelper.eventReminder, "")
            }
            this.contentResolver?.update(EventProvider.CONTENT_URI, cv, "eventID = ?", arrayOf(id))

            Toast.makeText(this, R.string.edit_successful, Toast.LENGTH_SHORT).show()
            val intents = Intent()
            setResult(Activity.RESULT_OK, intents)
            finish()
        }
    }

    /**
     * Event listener when click on button delete an event using its ID from the DB
     */
    fun deleteEvent(view: View) {
        this.contentResolver?.delete(EventProvider.CONTENT_URI, "eventID = ?", arrayOf(id))
        Toast.makeText(this, R.string.deleted, Toast.LENGTH_SHORT).show()

        val intents = Intent()
        setResult(Activity.RESULT_OK, intents)
        finish()
    }
}

