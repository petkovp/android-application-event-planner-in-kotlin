package com.androidteam.eventplanner;


import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.core.app.ActivityCompat;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.recyclerview.widget.RecyclerView;

import java.lang.reflect.Array;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

/**
 * Adapter that extends the recycler view used to add the list of events and sets a long click event listener that
 * will use intent to enter another activity
 */
public class EventListAdapter extends RecyclerView.Adapter<EventListAdapter.EventViewHolder> {
    private Context context;
    private LiveData<ArrayList<EventObject>> liveListEvent;
    private ListEventsFragment listEventsFragment;

    /**
     * Custom constructor
     *
     * @param context
     * @param listEvent
     * @param listEventsFragment
     */
    public EventListAdapter(Context context, LiveData<ArrayList<EventObject>> listEvent, ListEventsFragment listEventsFragment) {
        this.context = context;
        this.liveListEvent = listEvent;
        this.listEventsFragment = listEventsFragment;
    }

    /**
     * Adds a view to the eventViewHolder
     *
     * @param parent
     * @param viewType
     * @return
     */
    @Override
    public EventViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_layout, parent, false);
        EventViewHolder eventViewHolder = new EventViewHolder(view);
        return eventViewHolder;
    }

    /**
     * Adds the events on bind and has event listener onLongClick on event that will use intent to enter another custom activity
     *
     * @param holder
     * @param position
     */
    @Override
    public void onBindViewHolder(@NonNull EventViewHolder holder, int position) {
        ArrayList<EventObject> listEvent = liveListEvent.getValue();
        EventObject eventObj = listEvent.get(position);
        String eventStr = context.getText(R.string.event) + listEvent.get(position).getEventName() + "\n";
        if (listEvent.get(position).getEventDesc() != null && !listEvent.get(position).getEventDesc().isEmpty()) {
            eventStr = eventStr + context.getString(R.string.description) + listEvent.get(position).getEventDesc() + "\n";
        }
        else {
            eventStr = eventStr + context.getString(R.string.description) + context.getString(R.string.none) + "\n";
        }
        eventStr = eventStr + context.getString(R.string.importance) + listEvent.get(position).getEventImp() + "\n"
                + context.getString(R.string.date) + listEvent.get(position).getEventDate();
        if (listEvent.get(position).getReminderDate() == null) {
            eventStr = eventStr + "\n" +  context.getString(R.string.reminder) + context.getString(R.string.none);
        }
        else {
            eventStr = eventStr + "\n" +  context.getString(R.string.reminder) + listEvent.get(position).getReminderDate();
        }
        holder.textView.setText(eventStr);

        // When long click on event, intent to another activity where you can edit or delete event
        holder.textView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                Intent i = new Intent(context, EditEventActivity.class);
                i.putExtra("id", eventObj.getEventID().toString());
                i.putExtra("name", eventObj.getEventName());
                i.putExtra("desc", eventObj.getEventDesc());
                i.putExtra("imp", eventObj.getEventImp());
                i.putExtra("date", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(eventObj.getEventDate()));
                if (eventObj.getReminderDate() != null) {
                    i.putExtra("reminder", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(eventObj.getReminderDate()));
                }
                listEventsFragment.callIntent(i);
                return false;
            }
        });
    }

    /**
     * Gets the number of events
     *
     * @return nu8mber of events
     */
    @Override
    public int getItemCount() {
        return liveListEvent.getValue().size();
    }


    /**
     * ViewHolder class
     */
    public class EventViewHolder extends RecyclerView.ViewHolder {

        private TextView textView;

        /**
         * Sets textview on findViewById
         *
         * @param itemView
         */
        public EventViewHolder(@NonNull View itemView) {
            super(itemView);
            textView = itemView.findViewById(R.id.display_event);
        }
    }
}

