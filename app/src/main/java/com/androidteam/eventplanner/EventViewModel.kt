package com.androidteam.eventplanner;

import android.database.Cursor
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

/**
 * View model to share live data between fragments
 */
class EventViewModel : ViewModel() {
    val mutableListEvent = MutableLiveData<ArrayList<EventObject>>()

    /**
     * Sets the event list
     */
    fun setEventList(eventList : ArrayList<EventObject>) {
        mutableListEvent.value = eventList
    }

    /**
     * Gets the list of events
     */
    fun getEventList(): LiveData<ArrayList<EventObject>> {
        return this.mutableListEvent
    }
}
