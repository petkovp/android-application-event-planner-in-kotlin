package com.androidteam.eventplanner

import android.content.ContentValues
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.ViewModelProviders
import java.text.SimpleDateFormat
import java.time.LocalDate
import java.time.ZoneId
import java.util.*
import kotlin.Result.Companion.success

/**
 * The fragment(second tab) primary function to create event and store it into the database
 */
class CreateEventFragment : Fragment() {
    private lateinit var createEventView: View
    private val eventModel: EventViewModel by activityViewModels()

    /**
     * This inflates and returns the fragment view
     */
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        createEventView = inflater.inflate(R.layout.fragment_add_new_event, container, false)
        return createEventView
    }

    /**
     * manages event creation,
     * adds the events to the event list
     */
    @RequiresApi(Build.VERSION_CODES.M)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val spinner: Spinner = createEventView.findViewById(R.id.importance_spinner)
        // Create an ArrayAdapter using the string array and a default spinner layout
        activity?.let {
            ArrayAdapter.createFromResource(
                it,
                R.array.importance_array,
                android.R.layout.simple_spinner_item
            ).also { adapter ->
                // Specify the layout to use when the list of choices appears
                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                // Apply the adapter to the spinner
                spinner.adapter = adapter
            }
            createEventView.findViewById<TimePicker>(R.id.timePickerId).setIs24HourView(true)
            createEventView.findViewById<TimePicker>(R.id.reminderTimePickerId).setIs24HourView(true)

            // Event listener for when the user clicks "Create Event"
            createEventView.findViewById<Button>(R.id.createEventButt).setOnClickListener(View.OnClickListener {
                val eventName: EditText = createEventView.findViewById(R.id.eventNameId)
                val eventDesc: EditText = createEventView.findViewById(R.id.eventDescId)
                val eventImp: String = createEventView.findViewById<Spinner>(R.id.importance_spinner).selectedItem.toString()

                val datePicker = createEventView.findViewById<DatePicker>(R.id.datePickerId)
                val timePicker = createEventView.findViewById<TimePicker>(R.id.timePickerId)
                val reminderDatePicker = createEventView.findViewById<DatePicker>(R.id.reminderDatePickerId)
                val reminderTimePicker = createEventView.findViewById<TimePicker>(R.id.reminderTimePickerId)

                // Minus 1900 since the Date constructor for year automatically adds 1900
                val eventDate = Date(datePicker.year - 1900, datePicker.month, datePicker.dayOfMonth, timePicker.hour, timePicker.minute)
                val checkCheckBox = createEventView.findViewById<CheckBox>(R.id.checkboxReminderId).isChecked == true
                var reminderDate: Date? = null
                var checkReminderBeforeEventDate : Boolean = true
                if (checkCheckBox) {
                    reminderDate = Date(reminderDatePicker.year - 1900, reminderDatePicker.month, reminderDatePicker.dayOfMonth,
                        reminderTimePicker.hour, reminderTimePicker.minute)
                    checkReminderBeforeEventDate = reminderDate.before(eventDate) || reminderDate.equals(eventDate)
                }

                if (eventName.text.isBlank() || eventImp.isBlank()) {
                    Toast.makeText(context, R.string.must_fill_event, Toast.LENGTH_SHORT).show()
                }
                else if (!checkReminderBeforeEventDate) {
                    Toast.makeText(context, R.string.date_warning, Toast.LENGTH_SHORT).show()
                }
                else {
                    //Add Event to DB
                    val eventObj = EventObject(eventName.text.toString(), eventDesc.text.toString(), eventImp, eventDate, reminderDate)
                    addEventToDatabase(eventObj)

                    // READ ALL
                    var eventArr = arrayListOf<EventObject>()
                    var rs = activity?.contentResolver?.query(EventProvider.CONTENT_URI, arrayOf(DatabaseHelper.eventIDCOL, DatabaseHelper.eventName, DatabaseHelper.eventDesc,
                        DatabaseHelper.eventImportance, DatabaseHelper.eventDate, DatabaseHelper.eventReminder), null, null, null)
                    while (rs?.moveToNext()!!) {
                        Log.i("Event id", rs.getString(0))
                        Log.i("Event name", rs.getString(1))

                        val eventDate = SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(rs.getString(4))
                        var eventRem : Date? = null
                        if (rs.getString(5) != null && rs.getString(5).isNotEmpty()) {
                            eventRem = SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(rs.getString(5))
                        }
                        val eventObj = EventObject(rs.getString(1), rs.getString(2),
                            rs.getString(3), eventDate, eventRem)
                        eventObj.eventID = rs.getInt(0)
                        eventArr.add(eventObj)
                    }
                    eventModel.setEventList(eventArr)
                    Toast.makeText(context, R.string.success, Toast.LENGTH_SHORT).show()
                }
            })
        }
    }

    /**
     * Inserts the event created to the database
     */
    private fun addEventToDatabase(eventObj: EventObject){
        var cv = ContentValues()
        cv.put(DatabaseHelper.eventName, eventObj.eventName)
        cv.put(DatabaseHelper.eventDesc, eventObj.eventDesc)
        cv.put(DatabaseHelper.eventImportance, eventObj.eventImp)

        val dateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        cv.put(DatabaseHelper.eventDate, dateFormat.format(eventObj.eventDate))

        if (eventObj.reminderDate != null) {
            cv.put(DatabaseHelper.eventReminder, dateFormat.format(eventObj.reminderDate))
        }
        else {
            cv.put(DatabaseHelper.eventReminder, "")
        }
        activity?.contentResolver?.insert(EventProvider.CONTENT_URI, cv)
    }
}