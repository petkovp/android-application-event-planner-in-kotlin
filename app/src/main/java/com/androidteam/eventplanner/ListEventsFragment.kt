package com.androidteam.eventplanner

import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import java.text.SimpleDateFormat
import java.util.*

/**
 * Fragment subclass for the listing of events as well as displaying event/reminder notification on application startup
 */
class ListEventsFragment : Fragment() {
    private lateinit var listEventView: View
    private val eventModel: EventViewModel by activityViewModels()
    lateinit var eventAdapter : EventListAdapter

    /**
     * Inflate the fragment view
     */
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        listEventView = inflater.inflate(R.layout.fragment_list_events, container, false)
        return listEventView
    }

    /**
     * overrides the onViewCreated function to add a layoutmanager and a recyclerView adapter
     */
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        // READ ALL
        readAllEvents()
        val recyclerView: RecyclerView = activity?.findViewById(R.id.list_recycler_view) as RecyclerView
        val layoutManager = LinearLayoutManager(activity?.applicationContext)
        recyclerView.layoutManager = layoutManager
        eventAdapter = EventListAdapter(activity, this.eventModel.getEventList(), this)
        recyclerView.adapter = eventAdapter
    }

    /**
     * StartAcitivity intent from this fragment
     */
    fun callIntent(intent: Intent) {
        startActivityForResult(intent, 1);
    }

    /**
     * Notify the EventListAdapter instance variable on change
     */
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        // onChange that notify data change in EventListAdapter
        eventModel.getEventList()?.observe(viewLifecycleOwner,
            Observer {
                Log.i("CHECK CHANGED", "" + (this.eventModel.getEventList().value?.size))
                eventAdapter.notifyDataSetChanged()
            }
        )
        checkIfEventPassed()
    }

    /**
     * On activity result will read all events and notify data change to EventListAdapter
     */
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        readAllEvents()
        eventAdapter.notifyDataSetChanged()
    }

    /**
     * Reads the events one by one
     */
    private fun readAllEvents() {
        // READ ALL
        var eventArr = arrayListOf<EventObject>()
        var rs = activity?.contentResolver?.query(EventProvider.CONTENT_URI, arrayOf(DatabaseHelper.eventIDCOL, DatabaseHelper.eventName, DatabaseHelper.eventDesc,
            DatabaseHelper.eventImportance, DatabaseHelper.eventDate, DatabaseHelper.eventReminder), null, null, null)
        while (rs?.moveToNext()!!) {
            Log.i("Event id", rs.getString(0))
            Log.i("Event name", rs.getString(1))
            val eventDate = SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(rs.getString(4))
            var eventRem : Date? = null
            if (rs.getString(5) != null && rs.getString(5).isNotEmpty()) {
                eventRem = SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(rs.getString(5))
            }
            val eventObj = EventObject(rs.getString(1), rs.getString(2),
                rs.getString(3), eventDate, eventRem)
            eventObj.eventID = rs.getInt(0)
            eventArr.add(eventObj)
        }
        eventModel.setEventList(eventArr)
    }

    /**
     * Checks whether an event or reminder has passed from current date whenver the application opens
     */
    private fun checkIfEventPassed(){
        var checkEventPassed : Boolean = false

        //Iterate through recyclerview and check if current date is over an event date
        var counter = 0
        var resultSet = activity?.contentResolver?.query(EventProvider.CONTENT_URI, arrayOf(DatabaseHelper.eventIDCOL, DatabaseHelper.eventName, DatabaseHelper.eventDesc,
            DatabaseHelper.eventImportance, DatabaseHelper.eventDate, DatabaseHelper.eventReminder), null, null, null)
        val dateFormat = SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
        val tempStringDate = dateFormat.format(Date())
        val tempDate = dateFormat.parse(tempStringDate)

        while (resultSet?.moveToNext()!!) {
            var eventObj = EventObject("", "", "", tempDate, tempDate)
            val eventDate = dateFormat.parse(resultSet.getString(4))
            val stringDate = dateFormat.format(Date())
            val currentDate = dateFormat.parse(stringDate)
            var reminderDate : Date? = null
            if (resultSet.getString(5) != null && resultSet.getString(5).isNotEmpty())
                reminderDate = dateFormat.parse(resultSet.getString(5))

            if (eventDate.before(currentDate)){
                showDialogEvent(resultSet.getInt(0), resultSet.getString(1))
            }
            else if (reminderDate != null) {
                if (reminderDate.before(currentDate)) {
                    checkEventPassed = true;
                    val eventDateTemp = SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(resultSet.getString(4))
                    var eventRem : Date? = null
                    if (resultSet.getString(5) != null && resultSet.getString(5).isNotEmpty()) {
                        eventRem = SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(resultSet.getString(5))
                    }
                    eventObj = EventObject(
                        resultSet.getString(1), resultSet.getString(2),
                        resultSet.getString(3), eventDateTemp, eventRem
                    )
                    eventObj.eventID = resultSet.getInt(0)
                    if (checkEventPassed) {
                        showDialogReminder(eventObj)
                    }
                }
            }
            counter += 1
        }
    }

    /**
     * Displays dialog as a notification when an event date has passed
     */
    private fun showDialogEvent(eventId : Int, eventName : String) {
        val dialogClickListener =
            DialogInterface.OnClickListener { dialog, which ->
                when (which) {
                    DialogInterface.BUTTON_POSITIVE -> {
                        activity?.contentResolver?.delete(EventProvider.CONTENT_URI, "eventID = ?", arrayOf(eventId.toString()))
                        readAllEvents()
                    }
                }
            }
        val builder: AlertDialog.Builder = AlertDialog.Builder(context)
        val s1 = getString(R.string.event_passed)
        val s2 = getString(R.string.EVENT_notif)
        builder.setMessage("$s2 $eventName \n$s1" ).setPositiveButton(
            R.string.Yes,
            dialogClickListener
        )
            .setNegativeButton(R.string.no, dialogClickListener).show()
    }

    /**
     * Displays dialog as a notification when a reminder has passed
     */
    private fun showDialogReminder(eventObj : EventObject) {
        val dialogClickListener =
            DialogInterface.OnClickListener { dialog, which ->
                when (which) {
                    DialogInterface.BUTTON_POSITIVE -> {
                        val intent = Intent(context, EditEventActivity::class.java)
                        intent.putExtra("id", eventObj.eventID.toString())
                        intent.putExtra("name", eventObj.eventName)
                        intent.putExtra("desc", eventObj.eventDesc)
                        intent.putExtra("imp", eventObj.eventImp)
                        intent.putExtra("date", SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(eventObj.eventDate))
                        if (eventObj.reminderDate != null) {
                            intent.putExtra("reminder", SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(eventObj.reminderDate))
                        }
                        startActivityForResult(intent, 1)
                    }
                }
            }
        val builder: AlertDialog.Builder = AlertDialog.Builder(context)
        val s1 = getString(R.string.another_reminder)
        val s2 = getString(R.string.EVENT_REMINDER)
        builder.setMessage("$s2 ${eventObj.eventName} \n$s1").setPositiveButton(
            R.string.Yes,
            dialogClickListener
        )
            .setNegativeButton(R.string.no, dialogClickListener).show()
    }
}